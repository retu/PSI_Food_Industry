/**
 * 运单管理 - 主界面
 */
Ext.define("PSI.Express.MainForm", {
	extend : "Ext.panel.Panel",
	border : 0,
	layout : "border",

	config : {
		pAdd : null,
		pEdit : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		Ext.apply(me, {
					tbar : [{
								text : "寄件",
								disabled : me.getPAdd() == "0",
								iconCls : "PSI-button-add",
								handler : me.onAddExpress,
								scope : me
							}, "-", {
								text : "运单变更",
								iconCls : "PSI-button-edit",
								disabled : me.getPEdit() == "0",
								handler : me.onEditExpress,
								scope : me
							}, "-", {
								text : "关闭",
								iconCls : "PSI-button-exit",
								handler : function() {
									location.replace(PSI.Const.BASE_URL);
								}
							}],
					items : [{
								region : "north",
								height : 60,
								layout : "fit",
								border : 0,
								title : "查询条件",
								collapsible : true,
								layout : {
									type : "table",
									columns : 4
								},
								items : me.getQueryCmp()
							}, {
								region : "center",
								layout : "border",
								border : 0,
								items : [{
											region : "center",
											layout : "fit",
											items : [me.getMainGrid()]
										}, {
											region : "south",
											layout : "fit",
											height : "50%",
											split : true,
											items : [me.getDetailGrid()]
										}]
							}]
				});

		me.callParent(arguments);
	},

	/**
	 * 寄件
	 */
	onAddExpress : function() {
		var me = this;
		var form = Ext.create("PSI.Express.AddForm", {
					parentForm : me
				});
		form.show();
	},

	/**
	 * 运单变更
	 */
	onEditExpress : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择要变更的运单");
			return;
		}
		var bill = item[0];

		var form = Ext.create("PSI.Express.EditForm", {
					parentForm : me,
					entity : bill
				});
		form.show();
	},

	getMainGrid : function() {
		var me = this;
		if (me.__mainGrid) {
			return me.__mainGrid;
		}

		var modelName = "PSIExpressBill";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "ref", "status", "recName", "recAddress",
							"recTel", "sendName", "sendAddress", "sendTel",
							"cargo", "isFragile", "billDT", "freight",
							"payment", "claimValue", "cargoMoney", "memo",
							"startPlace", "destPlace"]
				});

		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : [],
					pageSize : 20,
					proxy : {
						type : "ajax",
						actionMethods : {
							read : "POST"
						},
						url : PSI.Const.BASE_URL + "Home/Express/expressList",
						reader : {
							root : 'dataList',
							totalProperty : 'totalCount'
						}
					}
				});
		store.on("beforeload", function() {
					store.proxy.extraParams = me.getQueryParam();
				});
		store.on("load", function(e, records, successful) {
					if (successful) {
						me.gotoMainGridRecord(me.__lastId);
					}
				});

		me.__mainGrid = Ext.create("Ext.grid.Panel", {
					border : 0,
					viewConfig : {
						enableTextSelection : true
					},
					columnLines : true,
					columns : [{
								header : "状态",
								dataIndex : "status",
								menuDisabled : true,
								sortable : false
							}, {
								header : "运单号",
								dataIndex : "ref",
								menuDisabled : true,
								sortable : false
							}, {
								header : "目的地",
								dataIndex : "destPlace",
								menuDisabled : true,
								sortable : false
							}, {
								header : "始发地",
								dataIndex : "startPlace",
								menuDisabled : true,
								sortable : false
							}, {
								header : "收镖人姓名",
								dataIndex : "recName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "收镖人地址",
								dataIndex : "recAddress",
								menuDisabled : true,
								sortable : false
							}, {
								header : "收镖人电话",
								dataIndex : "recTel",
								menuDisabled : true,
								sortable : false
							}, {
								header : "托镖人姓名",
								dataIndex : "sendName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "托镖人地址",
								dataIndex : "sendAddress",
								menuDisabled : true,
								sortable : false
							}, {
								header : "托镖人电话",
								dataIndex : "sendTel",
								menuDisabled : true,
								sortable : false
							}, {
								header : "镖物",
								dataIndex : "cargo",
								menuDisabled : true,
								sortable : false
							}, {
								header : "勿压易碎",
								dataIndex : "isFragile",
								menuDisabled : true,
								sortable : false
							}, {
								header : "运费",
								dataIndex : "freight",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn"
							}, {
								header : "付款方式",
								dataIndex : "payment",
								menuDisabled : true,
								sortable : false
							}, {
								header : "申明价值",
								dataIndex : "claimValue",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn"
							}, {
								header : "代收货款",
								dataIndex : "cargoMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn"
							}, {
								header : "运单日期",
								dataIndex : "billDT",
								menuDisabled : true,
								sortable : false
							}, {
								header : "备注",
								dataIndex : "memo",
								menuDisabled : true,
								sortable : false
							}],
					store : store,
					tbar : [{
								id : "pagingToobar",
								xtype : "pagingtoolbar",
								border : 0,
								store : store
							}, "-", {
								xtype : "displayfield",
								value : "每页显示"
							}, {
								id : "comboCountPerPage",
								xtype : "combobox",
								editable : false,
								width : 60,
								store : Ext.create("Ext.data.ArrayStore", {
											fields : ["text"],
											data : [["20"], ["50"], ["100"],
													["300"], ["1000"]]
										}),
								value : 20,
								listeners : {
									change : {
										fn : function() {
											store.pageSize = Ext
													.getCmp("comboCountPerPage")
													.getValue();
											store.currentPage = 1;
											Ext.getCmp("pagingToobar")
													.doRefresh();
										},
										scope : me
									}
								}
							}, {
								xtype : "displayfield",
								value : "条记录"
							}],
					listeners : {
						itemdblclick : {
							fn : me.onEditExpress,
							scope : me
						},
						select : {
							fn : me.onMainGridSelect,
							scope : me
						}
					}
				});

		return me.__mainGrid;
	},

	/**
	 * 查询条件
	 */
	getQueryCmp : function() {
		var me = this;
		return [{
			id : "editQueryBillStatus",
			xtype : "combo",
			queryMode : "local",
			editable : false,
			valueField : "id",
			labelWidth : 60,
			labelAlign : "right",
			labelSeparator : "",
			fieldLabel : "状态",
			margin : "5, 0, 0, 0",
			store : Ext.create("Ext.data.ArrayStore", {
						fields : ["id", "text"],
						data : [[-1, "全部"], [100, "待收件"], [200, "已收件"],
								[300, "货物达到仓库"], [400, "派送中"], [500, "已签收"]]
					}),
			value : -1
		}, {
			id : "editQueryRef",
			labelWidth : 60,
			labelAlign : "right",
			labelSeparator : "",
			fieldLabel : "运单号",
			margin : "5, 0, 0, 0",
			xtype : "textfield"
		}, {
			xtype : "container",
			items : [{
						xtype : "button",
						text : "查询",
						width : 100,
						margin : "5 0 0 10",
						iconCls : "PSI-button-refresh",
						handler : me.onQuery,
						scope : me
					}, {
						xtype : "button",
						text : "清空查询条件",
						width : 100,
						margin : "5, 0, 0, 10",
						handler : me.onClearQuery,
						scope : me
					}]
		}];
	},

	/**
	 * 查询
	 */
	onQuery : function() {
		var me = this;
		me.getMainGrid().getStore().removeAll();
		me.getDetailGrid().getStore().removeAll();

		Ext.getCmp("pagingToobar").doRefresh();
	},

	/**
	 * 清除查询条件
	 */
	onClearQuery : function() {
		var me = this;

		Ext.getCmp("editQueryBillStatus").setValue(-1);
		Ext.getCmp("editQueryRef").setValue(null);

		me.onQuery();
	},

	getQueryParam : function() {
		var me = this;

		var result = {
			billStatus : Ext.getCmp("editQueryBillStatus").getValue()
		};

		var ref = Ext.getCmp("editQueryRef").getValue();
		if (ref) {
			result.ref = ref;
		}

		return result;
	},

	onMainGridSelect : function() {
		var me = this;
		me.getDetailGrid().setTitle("运单变更详情");
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return;
		}

		me.refreshDetailGrid();
	},

	getDetailGrid : function() {
		var me = this;
		if (me.__detailGrid) {
			return me.__detailGrid;
		}

		var modelName = "PSIExpressBillTracing";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["ref", "statusBefore", "statusAfter",
							"changedDT", "userName", "memo"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : []
				});

		me.__detailGrid = Ext.create("Ext.grid.Panel", {
					title : "运单变更详情",
					viewConfig : {
						enableTextSelection : true
					},
					columnLines : true,
					columns : [{
								header : "单号",
								dataIndex : "ref",
								menuDisabled : true,
								sortable : false
							}, {
								header : "变更前状态",
								dataIndex : "statusBefore",
								menuDisabled : true,
								sortable : false,
								width : 200
							}, {
								header : "变更后状态",
								dataIndex : "statusAfter",
								menuDisabled : true,
								sortable : false,
								width : 200
							}, {
								header : "变更时间",
								dataIndex : "changedDT",
								menuDisabled : true,
								sortable : false,
								width : 150
							}, {
								header : "变更人",
								dataIndex : "userName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "备注",
								dataIndex : "memo",
								menuDisabled : true,
								sortable : false,
								width : 250
							}],
					store : store
				});

		return me.__detailGrid;
	},

	refreshDetailGrid : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return;
		}
		var bill = item[0];

		var grid = me.getDetailGrid();
		var el = grid.getEl();
		el.mask(PSI.Const.LOADING);

		var r = {
			url : PSI.Const.BASE_URL + "Home/Express/expressTracing",
			params : {
				id : bill.get("id")
			},
			method : "POST",
			callback : function(options, success, response) {
				var store = grid.getStore();

				store.removeAll();

				if (success) {
					var data = Ext.JSON.decode(response.responseText);
					store.add(data);
				}

				el.unmask();
			}
		};
		Ext.Ajax.request(r);
	},

	gotoMainGridRecord : function(id) {
		var me = this;
		var grid = me.getMainGrid();
		var store = grid.getStore();
		if (id) {
			var r = store.findExact("id", id);
			if (r != -1) {
				grid.getSelectionModel().select(r);
			} else {
				grid.getSelectionModel().select(0);
			}
		}
	},

	refreshEditResult : function(id) {
		var me = this;
		me.__lastId = id;
		me.onQuery();
	}
});