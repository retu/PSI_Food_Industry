/**
 * 人员职务 - 新增或编辑界面
 */
Ext.define("PSI.UserPosition.EditForm", {
			extend : "Ext.window.Window",

			config : {
				parentForm : null,
				entity : null
			},

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				var entity = me.getEntity();

				me.adding = entity == null;

				var modelName = "PSICompany";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "name"]
						});

				var companyStore = Ext.create("Ext.data.Store", {
							model : modelName,
							autoLoad : false,
							data : []
						});
				me.companyStore = companyStore;

				var buttons = [];
				if (!entity) {
					var btn = {
						text : "保存并继续新增",
						formBind : true,
						handler : function() {
							me.onOK(true);
						},
						scope : me
					};

					buttons.push(btn);
				}

				var btn = {
					text : "保存",
					formBind : true,
					iconCls : "PSI-button-ok",
					handler : function() {
						me.onOK(false);
					},
					scope : me
				};
				buttons.push(btn);

				var btn = {
					text : entity == null ? "关闭" : "取消",
					handler : function() {
						me.close();
					},
					scope : me
				};
				buttons.push(btn);

				Ext.apply(me, {
							title : entity == null ? "新增职务" : "编辑职务",
							modal : true,
							resizable : false,
							onEsc : Ext.emptyFn,
							width : 400,
							height : 140,
							layout : "fit",
							listeners : {
								show : {
									fn : me.onWndShow,
									scope : me
								},
								close : {
									fn : me.onWndClose,
									scope : me
								}
							},
							items : [{
								id : "editForm",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 60,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side',
									width : 370,
									margin : "5"
								},
								items : [{
									xtype : "hidden",
									name : "id",
									value : entity == null ? null : entity
											.get("id")
								}, {
									id : "editName",
									fieldLabel : "职务",
									allowBlank : false,
									blankText : "没有输入职务",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									name : "name",
									value : entity == null ? null : entity
											.get("name"),
									listeners : {
										specialkey : {
											fn : me.onEditNameSpecialKey,
											scope : me
										}
									}
								}, {
									id : "editCompany",
									xtype : "combo",
									fieldLabel : "所属公司",
									allowBlank : false,
									blankText : "没有输入所属公司",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									valueField : "id",
									displayField : "name",
									store : me.companyStore,
									queryMode : "local",
									editable : false,
									name : "companyId"
								}],
								buttons : buttons
							}]
						});

				me.callParent(arguments);
			},

			/**
			 * 保存
			 */
			onOK : function(thenAdd) {
				var me = this;
				var f = Ext.getCmp("editForm");
				var el = f.getEl();
				el.mask(PSI.Const.SAVING);
				var sf = {
					url : PSI.Const.BASE_URL + "Home/User/editUserPosition",
					method : "POST",
					success : function(form, action) {
						me.__lastId = action.result.id;

						el.unmask();

						PSI.MsgBox.tip("数据保存成功");
						me.focus();
						if (thenAdd) {
							me.clearEdit();
						} else {
							me.close();
						}
					},
					failure : function(form, action) {
						el.unmask();
						PSI.MsgBox.showInfo(action.result.msg, function() {
									Ext.getCmp("editName").focus();
								});
					}
				};
				f.submit(sf);
			},

			onEditNameSpecialKey : function(field, e) {
				var me = this;

				if (e.getKey() == e.ENTER) {
					var f = Ext.getCmp("editForm");
					if (f.getForm().isValid()) {
						me.onOK(me.adding);
					}
				}
			},

			clearEdit : function() {
				var editors = [Ext.getCmp("editName")];
				for (var i = 0; i < editors.length; i++) {
					var edit = editors[i];
					edit.setValue(null);
					edit.clearInvalid();
				}
			},

			onWndClose : function() {
				var me = this;
				if (me.__lastId) {
					me.getParentForm().freshGrid(me.__lastId);
				}
			},

			onWndShow : function() {
				var edit = Ext.getCmp("editName");
				if (edit) {
					edit.focus();
				}

				var me = this;

				var id = null;
				if (me.getEntity()) {
					id = me.getEntity().get("id");
				}

				var el = me.getEl();
				var store = me.companyStore;
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL
									+ "Home/User/getCompanyList",
							params : {
								id : id
							},
							method : "POST",
							callback : function(options, success, response) {
								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									var companyList = data.companyList;
									store.add(companyList);

									if (data.name) {
										edit.setValue(data.name);
									}

									var editCompany = Ext.getCmp("editCompany");
									if (data.companyId) {
										editCompany.setValue(data.companyId);
									} else {
										if (companyList.length > 0) {
											editCompany
													.setValue(companyList[0].id);
										}
									}
								}

								el.unmask();
							}
						});
			}
		});