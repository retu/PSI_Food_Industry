/**
 * 销售收入方式 - 主界面
 */
Ext.define("PSI.SaleOrder.SalesModeMainForm", {
	extend : "Ext.panel.Panel",
	border : 0,
	layout : "border",

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		Ext.apply(me, {
					tbar : [{
								text : "新增收入方式",
								iconCls : "PSI-button-add",
								handler : me.onAddMode,
								scope : me
							}, {
								text : "编辑收入方式",
								iconCls : "PSI-button-edit",
								handler : me.onEditMode,
								scope : me
							}, {
								text : "删除收入方式",
								iconCls : "PSI-button-delete",
								handler : me.onDeleteMode,
								scope : me
							}, "-", {
								text : "关闭",
								iconCls : "PSI-button-exit",
								handler : function() {
									location.replace(PSI.Const.BASE_URL);
								}
							}],
					items : [{
								region : "center",
								xtype : "panel",
								layout : "fit",
								border : 0,
								items : [me.getMainGrid()]
							}]
				});

		me.callParent(arguments);

		me.freshGrid();
	},

	/**
	 * 新增收入方式
	 */
	onAddMode : function() {
		var form = Ext.create("PSI.SaleOrder.SalesModeEditForm", {
					parentForm : this
				});

		form.show();
	},

	/**
	 * 编辑收入方式
	 */
	onEditMode : function() {
		var me = this;

		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择要编辑的收入方式");
			return;
		}

		var mode = item[0];

		var form = Ext.create("PSI.SaleOrder.SalesModeEditForm", {
					parentForm : me,
					entity : mode
				});

		form.show();
	},

	/**
	 * 删除收入方式
	 */
	onDeleteMode : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择要删除的收入方式");
			return;
		}

		var mode = item[0];
		var info = "请确认是否删除收入方式 <span style='color:red'>" + mode.get("name")
				+ "</span> ?";

		var store = me.getMainGrid().getStore();
		var index = store.findExact("id", mode.get("id"));
		index--;
		var preIndex = null;
		var pre = store.getAt(index);
		if (pre) {
			preIndex = pre.get("id");
		}

		var funcConfirm = function() {
			var el = Ext.getBody();
			el.mask(PSI.Const.LOADING);
			var r = {
				url : PSI.Const.BASE_URL + "Home/Sale/deleteSalesMode",
				params : {
					id : mode.get("id")
				},
				method : "POST",
				callback : function(options, success, response) {
					el.unmask();
					if (success) {
						var data = Ext.JSON.decode(response.responseText);
						if (data.success) {
							PSI.MsgBox.tip("成功完成删除操作");
							me.freshGrid(preIndex);
						} else {
							PSI.MsgBox.showInfo(data.msg);
						}
					} else {
						PSI.MsgBox.showInfo("网络错误");
					}
				}
			};

			Ext.Ajax.request(r);
		};

		PSI.MsgBox.confirm(info, funcConfirm);
	},

	freshGrid : function(id) {
		var me = this;
		var grid = me.getMainGrid();
		var el = grid.getEl() || Ext.getBody();
		el.mask(PSI.Const.LOADING);
		Ext.Ajax.request({
					url : PSI.Const.BASE_URL + "Home/Sale/salesModeList",
					method : "POST",
					callback : function(options, success, response) {
						var store = grid.getStore();

						store.removeAll();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							store.add(data);

							me.gotoGridRecord(id);
						}

						el.unmask();
					}
				});
	},

	gotoGridRecord : function(id) {
		var me = this;
		var grid = me.getMainGrid();
		var store = grid.getStore();
		if (id) {
			var r = store.findExact("id", id);
			if (r != -1) {
				grid.getSelectionModel().select(r);
			} else {
				grid.getSelectionModel().select(0);
			}
		}
	},

	getMainGrid : function() {
		var me = this;
		if (me.__mainGrid) {
			return me.__mainGrid;
		}

		var modelName = "PSISalesMode";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "modeType", "modeTypeId", "name"]
				});

		me.__mainGrid = Ext.create("Ext.grid.Panel", {
					border : 0,
					viewConfig : {
						enableTextSelection : true
					},
					columnLines : true,
					columns : [{
								xtype : "rownumberer"
							}, {
								header : "分类",
								dataIndex : "modeType",
								menuDisabled : true,
								sortable : false,
								width : 120
							}, {
								header : "收入方式",
								dataIndex : "name",
								menuDisabled : true,
								sortable : false,
								width : 200
							}],
					store : Ext.create("Ext.data.Store", {
								model : modelName,
								autoLoad : false,
								data : []
							}),
					listeners : {
						itemdblclick : {
							fn : me.onEditWarehouse,
							scope : me
						}
					}
				});

		return me.__mainGrid;
	}
});