/**
 * 商品组合 - 设置界面
 */
Ext.define("PSI.Goods.GoodsBOMEditForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null,
		entity : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;
		var entity = me.getEntity();

		Ext.apply(me, {
					title : "设置商品组合",
					modal : true,
					onEsc : Ext.emptyFn,
					width : 900,
					height : 500,
					layout : "border",
					items : [{
								region : "center",
								border : 0,
								bodyPadding : 10,
								layout : "fit",
								items : [me.getGoodsGrid()]
							}, {
								region : "north",
								border : 0,
								layout : {
									type : "table",
									columns : 2
								},
								height : 40,
								bodyPadding : 10,
								items : [{
											xtype : "hidden",
											id : "hiddenId",
											name : "id",
											value : entity.get("id")
										}, {
											id : "editRef",
											fieldLabel : "父商品",
											labelWidth : 60,
											labelAlign : "right",
											labelSeparator : ":",
											xtype : "displayfield",
											value : entity.get("code") + " "
													+ entity.get("name") + " "
													+ entity.get("spec")
										}]
							}],
					buttons : [{
								text : "保存",
								iconCls : "PSI-button-ok",
								handler : me.onOK,
								scope : me,
								id : "buttonSave"
							}, {
								text : "取消",
								handler : function() {
									PSI.MsgBox.confirm("请确认是否取消当前操作？",
											function() {
												me.close();
											});
								},
								scope : me,
								id : "buttonCancel"
							}],
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						}
					}
				});

		me.callParent(arguments);
	},

	onWndShow : function() {
		var me = this;

		var el = me.getEl() || Ext.getBody();
		el.mask(PSI.Const.LOADING);
		Ext.Ajax.request({
					url : PSI.Const.BASE_URL + "Home/Goods/goodsBOMInfo",
					params : {
						id : Ext.getCmp("hiddenId").getValue()
					},
					method : "POST",
					callback : function(options, success, response) {
						el.unmask();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);

							var store = me.getGoodsGrid().getStore();
							store.removeAll();
							store.add(data);
							if (data.length == 0) {
								store.add({
											goodsCount : 1
										});
							}
							me.getGoodsGrid().focus();
							me.__cellEditing.startEdit(0, 0);
						} else {
							PSI.MsgBox.showInfo("网络错误")
						}
					}
				});
	},

	onOK : function() {
		var me = this;
		Ext.getBody().mask("正在保存中...");
		Ext.Ajax.request({
					url : PSI.Const.BASE_URL + "Home/Goods/editGoodsBOM",
					method : "POST",
					params : {
						jsonStr : me.getSaveData()
					},
					callback : function(options, success, response) {
						Ext.getBody().unmask();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							if (data.success) {
								PSI.MsgBox.showInfo("成功保存数据", function() {
											me.close();
											me.getParentForm().onGoodsSelect();
										});
							} else {
								PSI.MsgBox.showInfo(data.msg);
							}
						}
					}
				});
	},

	getGoodsGrid : function() {
		var me = this;
		if (me.__goodsGrid) {
			return me.__goodsGrid;
		}
		var modelName = "PSIGoodsBOM_EditForm";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "code", "name", "spec", "goodsCount",
							"unitName"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : []
				});

		me.__cellEditing = Ext.create("PSI.UX.CellEditing", {
					clicksToEdit : 1,
					listeners : {
						edit : {
							fn : me.cellEditingAfterEdit,
							scope : me
						}
					}
				});

		me.__goodsGrid = Ext.create("Ext.grid.Panel", {
					viewConfig : {
						enableTextSelection : true
					},
					plugins : [me.__cellEditing],
					columnLines : true,
					columns : [{
								header : "子商品编码",
								dataIndex : "code",
								width : 120,
								menuDisabled : true,
								sortable : false,
								editor : {
									xtype : "psi_goodsbomfield",
									parentCmp : me,
									parentGoodsId : me.getEntity().get("id")
								}
							}, {
								header : "子商品名称",
								dataIndex : "name",
								width : 200,
								menuDisabled : true,
								sortable : false
							}, {
								header : "规格型号",
								dataIndex : "spec",
								width : 200,
								menuDisabled : true,
								sortable : false
							}, {
								header : "数量",
								dataIndex : "goodsCount",
								width : 120,
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn",
								format : "#",
								editor : {
									xtype : "numberfield",
									allowDecimals : false,
									hideTrigger : true
								}
							}, {
								header : "计量单位",
								dataIndex : "unitName",
								width : 80,
								menuDisabled : true,
								sortable : false
							}, {
								header : "",
								id : "columnActionDelete",
								align : "center",
								menuDisabled : true,
								draggable : false,
								width : 40,
								xtype : "actioncolumn",
								items : [{
									icon : PSI.Const.BASE_URL
											+ "Public/Images/icons/delete.png",
									handler : function(grid, row) {
										var store = grid.getStore();
										store.remove(store.getAt(row));
										if (store.getCount() == 0) {
											store.add({
														goodsCount : 1
													});
										}
									},
									scope : me
								}]
							}, {
								header : "",
								id : "columnActionAdd",
								align : "center",
								menuDisabled : true,
								draggable : false,
								width : 40,
								xtype : "actioncolumn",
								items : [{
									icon : PSI.Const.BASE_URL
											+ "Public/Images/icons/add.png",
									handler : function(grid, row) {
										var store = grid.getStore();
										store.insert(row, [{
															goodsCount : 1
														}]);
									},
									scope : me
								}]
							}, {
								header : "",
								id : "columnActionAppend",
								align : "center",
								menuDisabled : true,
								draggable : false,
								width : 40,
								xtype : "actioncolumn",
								items : [{
									icon : PSI.Const.BASE_URL
											+ "Public/Images/icons/add_detail.png",
									handler : function(grid, row) {
										var store = grid.getStore();
										store.insert(row + 1, [{
															goodsCount : 1
														}]);
									},
									scope : me
								}]
							}],
					store : store
				});

		return me.__goodsGrid;
	},

	cellEditingAfterEdit : function(editor, e) {
		var me = this;

		var fieldName = e.field;
		var goods = e.record;
		var oldValue = e.originalValue;
		if (fieldName == "goodsCount") {
			var store = me.getGoodsGrid().getStore();
			if (e.rowIdx == store.getCount() - 1) {
				store.add({
							goodsCount : 1
						});
			}
			e.rowIdx += 1;
			me.getGoodsGrid().getSelectionModel().select(e.rowIdx);
			me.__cellEditing.startEdit(e.rowIdx, 0);
		}
	},

	getSaveData : function() {
		var me = this;

		var result = {
			id : Ext.getCmp("hiddenId").getValue(),
			items : []
		};

		var store = me.getGoodsGrid().getStore();
		for (var i = 0; i < store.getCount(); i++) {
			var item = store.getAt(i);
			result.items.push({
						id : item.get("id"),
						goodsCount : item.get("goodsCount")
					});
		}

		return Ext.JSON.encode(result);
	},

	__setGoodsInfo : function(data) {
		var me = this;
		var item = me.getGoodsGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return;
		}
		var goods = item[0];

		goods.set("id", data.id);
		goods.set("code", data.code);
		goods.set("name", data.name);
		goods.set("unitName", data.unitName);
		goods.set("spec", data.spec);

	}
});