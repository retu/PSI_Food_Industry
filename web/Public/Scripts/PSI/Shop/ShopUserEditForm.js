/**
 * 设置店员
 */
Ext.define("PSI.Shop.ShopUserEditForm", {
			extend : "Ext.window.Window",
			config : {
				entity : null,
				parentForm : null
			},

			initComponent : function() {
				var me = this;
				var entity = me.getEntity();

				Ext.apply(me, {
							title : "设置店员",
							modal : true,
							resizable : false,
							onEsc : Ext.emptyFn,
							width : 700,
							height : 600,
							layout : "border",
							items : [{
								xtype : "panel",
								region : "north",
								layout : "fit",
								height : 60,
								border : 0,
								items : [{
									id : "editForm",
									xtype : "form",
									layout : {
										type : "table",
										columns : 1
									},
									border : 0,
									bodyPadding : 5,
									items : [{
												xtype : "hidden",
												name : "id",
												value : entity.get("id")
											}, {
												xtype : "panel",
												border : 0,
												html : "<h1>"
														+ entity.get("name")
														+ "</h1>"
											}, {
												id : "editUserIdList",
												xtype : "hidden",
												name : "userIdList"
											}]
								}]
							}, {
								xtype : "panel",
								region : "center",
								border : 0,
								layout : "fit",
								items : [me.getUserGrid()]
							}],
							buttons : [{
										text : "确定",
										formBind : true,
										iconCls : "PSI-button-ok",
										handler : me.onOK,
										scope : me
									}, {
										text : "取消",
										handler : function() {
											PSI.MsgBox.confirm("请确认是否取消操作?",
													function() {
														me.close();
													});
										},
										scope : me
									}]
						});

				me.on("show", me.onWndShow, me);

				me.callParent(arguments);
			},

			onWndShow : function() {
				var me = this;
				
				var grid = me.getUserGrid();
				var el = grid.getEl();
				el.mask(PSI.Const.LOADING);

				var r = {
					url : PSI.Const.BASE_URL + "Home/Shop/userListForEdit",
					params : {
						id : me.getEntity().get("id")
					},
					method : "POST",
					callback : function(options, success, response) {
						var store = grid.getStore();

						store.removeAll();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							store.add(data);
						}

						el.unmask();
					}
				};
				Ext.Ajax.request(r);

			},

			setSelectedUsers : function(data) {
				var me = this;
				var store = me.getUserGrid().getStore();

				for (var i = 0; i < data.length; i++) {
					var item = data[i];
					store.add({
								id : item.get("id"),
								name : item.get("name"),
								loginName : item.get("loginName"),
								orgFullName : item.get("orgFullName")
							});
				}
			},

			onOK : function() {
				var me = this;

				var store = me.getUserGrid().getStore();
				var data = store.data;
				var idList = [];
				for (var i = 0; i < data.getCount(); i++) {
					var item = data.items[i].data;
					idList.push(item.id);
				}

				var editUserIdList = Ext.getCmp("editUserIdList");
				editUserIdList.setValue(idList.join());

				var editForm = Ext.getCmp("editForm");
				var el = this.getEl() || Ext.getBody();
				el.mask("数据保存中...");

				editForm.submit({
							url : PSI.Const.BASE_URL + "Home/Shop/editShopUser",
							method : "POST",
							success : function(form, action) {
								el.unmask();
								PSI.MsgBox.showInfo("数据保存成功", function() {
											me.close();
											me.getParentForm().freshUserGrid();
										});
							},
							failure : function(form, action) {
								el.unmask();
								PSI.MsgBox.showInfo(action.result.msg);
							}
						});
			},

			onAddUser : function() {
				var me = this;

				var store = me.getUserGrid().getStore();
				var data = store.data;
				var idList = [];
				for (var i = 0; i < data.getCount(); i++) {
					var item = data.items[i].data;
					idList.push(item.id);
				}

				var form = Ext.create("PSI.Shop.SelectUserForm", {
							idList : idList,
							parentForm : me
						});

				form.show();
			},

			onRemoveUser : function() {
				var me = this;

				var grid = me.getUserGrid();

				var items = grid.getSelectionModel().getSelection();
				if (items == null || items.length == 0) {
					PSI.MsgBox.showInfo("请选择要移除的人员");
					return;
				}

				grid.getStore().remove(items);
			},

			getUserGrid : function() {
				var me = this;
				if (me.__userGrid) {
					return me.__userGrid;
				}

				var modelName = "PSIShopUser_EditForm";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "loginName", "name", "orgFullName"]
						});

				var userStore = Ext.create("Ext.data.Store", {
							model : modelName,
							autoLoad : false,
							data : []
						});

				me.__userGrid = Ext.create("Ext.grid.Panel", {
							title : "属于当前店铺的店员",
							padding : 5,
							selModel : {
								mode : "MULTI"
							},
							selType : "checkboxmodel",
							store : userStore,
							columns : [{
										header : "用户姓名",
										dataIndex : "name",
										flex : 1
									}, {
										header : "登录名",
										dataIndex : "loginName",
										flex : 1
									}, {
										header : "所属组织",
										dataIndex : "orgFullName",
										flex : 1
									}, {
										header : "操作",
										align : "center",
										menuDisabled : true,
										width : 50,
										xtype : "actioncolumn",
										items : [{
											icon : PSI.Const.BASE_URL
													+ "Public/Images/icons/delete.png",
											handler : function(grid, row) {
												var store = grid.getStore();
												store.remove(store.getAt(row));
											},
											scope : me
										}]
									}

							],
							tbar : [{
										text : "添加用户",
										iconCls : "PSI-button-add",
										handler : me.onAddUser,
										scope : me
									}, "-", {
										text : "移除用户",
										iconCls : "PSI-button-delete",
										handler : me.onRemoveUser,
										scope : me
									}]
						});

				return me.__userGrid;
			}
		});