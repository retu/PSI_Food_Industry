﻿/**
 * 首页 - 待处理订单Portal
 * 
 * @author 李静波
 */
Ext.define("PSI.Home.ToDoOrderPortal", {
			extend : "Ext.panel.Panel",

			config : {},

			border : 0,
			bodyPadding : 5,

			initComponent : function() {
				var me = this;

				Ext.apply(me, {
							layout : "border",
							items : []
						});

				me.callParent(arguments);
			}
		});