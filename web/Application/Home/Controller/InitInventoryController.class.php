<?php

namespace Home\Controller;

use Think\Controller;
use Home\Service\InitInventoryService;
use Home\Service\ImportService;

/**
 * 库存建账Controller
 *
 * @author 李静波
 *        
 */
class InitInventoryController extends PSIBaseController {

	/**
	 * 查询仓库列表
	 */
	public function warehouseList() {
		if (IS_POST) {
			$is = new InitInventoryService();
			$this->ajaxReturn($is->warehouseList());
		}
	}

	/**
	 * 获得建账信息列表
	 */
	public function initInfoList() {
		if (IS_POST) {
			$params = array(
					"warehouseId" => I("post.warehouseId"),
					"page" => I("post.page"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->initInfoList($params));
		}
	}

	/**
	 * 录入建账信息时候，获得商品分类列表
	 */
	public function goodsCategoryList() {
		if (IS_POST) {
			$is = new InitInventoryService();
			$this->ajaxReturn($is->goodsCategoryList());
		}
	}

	/**
	 * 录入建账信息的时候，获得商品列表
	 */
	public function goodsList() {
		if (IS_POST) {
			$params = array(
					"warehouseId" => I("post.warehouseId"),
					"categoryId" => I("post.categoryId"),
					"page" => I("post.page"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->goodsList($params));
		}
	}

	/**
	 * 提交建账信息
	 */
	public function commitInitInventoryGoods() {
		if (IS_POST) {
			$params = array(
					"warehouseId" => I("post.warehouseId"),
					"goodsId" => I("post.goodsId"),
					"goodsCount" => I("post.goodsCount"),
					"goodsMoney" => I("post.goodsMoney")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->commitInitInventoryGoods($params));
		}
	}

	/**
	 * 标记完成建账
	 */
	public function finish() {
		if (IS_POST) {
			$params = array(
					"warehouseId" => I("post.warehouseId")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->finish($params));
		}
	}

	/**
	 * 取消建账完成标记
	 */
	public function cancel() {
		if (IS_POST) {
			$params = array(
					"warehouseId" => I("post.warehouseId")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->cancel($params));
		}
	}

	/**
	 * 通过导入万里牛库存数据建账
	 */
	public function importWLN() {
		if (IS_POST) {
			$upload = new \Think\Upload();
			
			// 允许上传的文件后缀
			$upload->exts = array(
					'xls',
					'xlsx'
			);
			
			// 保存路径
			$upload->savePath = '/InitInventory/';
			
			// 先上传文件
			$fileInfo = $upload->uploadOne($_FILES['data_file']);
			if (! $fileInfo) {
				$this->ajaxReturn(
						array(
								"msg" => $upload->getError(),
								"success" => false
						));
			} else {
				$uploadFileFullPath = './Uploads' . $fileInfo['savepath'] . $fileInfo['savename']; // 获取上传到服务器文件路径
				$uploadFileExt = $fileInfo['ext']; // 上传文件扩展名
				
				$params = array(
						"datafile" => $uploadFileFullPath,
						"ext" => $uploadFileExt
				);
				$cs = new ImportService();
				$this->ajaxReturn($cs->importWLNInitInventoryFromExcelFile($params));
			}
		}
	}

	/**
	 * 新增或编辑建账信息
	 */
	public function editInitInv() {
		if (IS_POST) {
			$params = array(
					"goodsId" => I("post.goodsId"),
					"warehouseId" => I("post.warehouseId"),
					"qcBeginDT" => I("post.qcBeginDT"),
					"expiration" => I("post.expiration"),
					"goodsCount" => I("post.count"),
					"goodsMoney" => I("post.money")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->editInitInv($params));
		}
	}

	/**
	 * 清空建账数据
	 */
	public function deleteInitData() {
		if (IS_POST) {
			$params = array(
					"warehouseId" => I("post.id")
			);
			
			$is = new InitInventoryService();
			$this->ajaxReturn($is->deleteInitData($params));
		}
	}

	/**
	 * 查询已经录入的某个商品的建账数据
	 */
	public function queryData() {
		if (IS_POST) {
			$params = array(
					"goodsId" => I("post.goodsId"),
					"warehouseId" => I("post.warehouseId"),
					"qcBeginDT" => I("post.qcBeginDT"),
					"expiration" => I("post.expiration")
			);
			$is = new InitInventoryService();
			$this->ajaxReturn($is->queryData($params));
		}
	}

	/**
	 * 建账数据导出Excel
	 */
	public function excel() {
		$warehouseId = I("get.id");
		
		$is = new InitInventoryService();
		$is->excel($warehouseId);
	}
}
