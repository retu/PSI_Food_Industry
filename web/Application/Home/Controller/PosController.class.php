<?php

namespace Home\Controller;

use Home\Common\FIdConst;
use Home\Service\UserService;
use Think\Controller;
use Home\Service\PosService;

/**
 * POS Controller
 *
 * @author 李静波
 *        
 */
class PosController extends PSIBaseController {

	/**
	 * 收银系统 - 主页面
	 */
	public function index() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::POS)) {
			$this->initVar();
			
			$this->assign("title", "收银系统");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Pos/index");
		}
	}

	/**
	 * 获得当前用户所有可以操作的店铺
	 */
	public function shopInfo() {
		if (IS_POST) {
			$ps = new PosService();
			$this->ajaxReturn($ps->shopInfo());
		}
	}

	/**
	 * 根据条码查询商品信息
	 */
	public function goodsByBarcode() {
		if (IS_POST) {
			$params = array(
					"barcode" => I("post.barcode")
			);
			
			$ps = new PosService();
			$this->ajaxReturn($ps->goodsByBarcode($params));
		}
	}

	public function commitBill() {
		if (IS_POST) {
			$params = array(
					"jsonStr" => I("post.jsonStr")
			);
			
			$ps = new PosService();
			$this->ajaxReturn($ps->commitBill($params));
		}
	}

	public function getPrintBillData() {
		if (IS_POST) {
			$id = I("post.id");
			
			$ps = new PosService();
			$data = $ps->getPrintBillData($id);
			$this->assign("data", $data);
			$this->display();
		}
	}
}